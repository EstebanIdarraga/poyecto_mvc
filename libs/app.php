<?php


//Archivo para el ruteo
require_once 'controllers/erroresController.php';

class App{

	function __construct(){

		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = explode('/', $url);


		//Valido si trae una ruta y si no la trae lo envia a uno ruta por defecto
		if(empty($url[0])){
			error_log('APP::construct-> No hay controlador especificado');

			$archivoController = 'controllers/loginController.php';
			require_once $archivoController;

			$controller = new LoginController();
			$controller->loadModel('login');
			$controller->render();

			return false;
		}




		//Si la url trae dtaos valido si este un metodo y parametros
		$archivoController = 'controllers/'.$url[0].'.php';

		if(file_exists($archivoController)){
			require_once $archivoController;

			$controller = new $url[0];
			$controller->loadModel($url[0]);

			if(isset($url[1])){
				 //Validos si traee metodos y parametros
			     if (method_exists($controller, $url[1])) {
			     	if($url[2]){
			     		//no hay parametros
			     		$nparam = count($url)-2;
			     		//Arreglo de parametros
			     		$params = [];

			     		for($i = 0; $i<$nparam; $i++){
			     			array_push($params, $url[$i] + 2);
			     		}

			     		$controller->{$url[1]}($params);

			     	}else{
			     		//Si no trae parametros llama al metodo
			     		$controller->{$url[1]}();
			     	}

			     } else {
			     	//Error, no exiate el metodo
			     	$controller = new ErroresController();
			     	$controller->render();

			     }

			}else{
				$controller->render();
			}

		}else{
			//No esiste el archivo
			$controller = new ErroresController();
			$controller->render();
		}
	}
}



 ?>