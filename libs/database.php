<?php

class Database{

	private $host;
	private $user;
	private $pass;
	private $dbname;
	private $charset;


	public function __construct(){

		$this->host = constant('HOST');
		$this->user = constant('USER');
		$this->pass = constant('PASS');
		$this->dbname = constant('DB');
		$this->charset = constant('CHARSET');
	}


	function connect(){
		try {
			$conn = "mysql:=host".$this->server.";dbname=".$this->dbname.";charset=".$this->charset;
			$opt = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES => false];

			$pdo = new PDO($conn, $this->user, $this->pass, $opt);

			error_log('Conexion exitosa !!');

			return $pdo;

		} catch (PDOException $e) {
			error_log('Error connection '.$e->getMessage());

		}
	}

}



 ?>