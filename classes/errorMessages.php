<?php

class ErrorMessages{

	//ERROR_CONTROLLER_METHOD_ACTION

	const PRUEBA = "eec8c35c345a1cd380c8cb539df27970";

	private $errorList = [];

	public function __construct(){
		$this->errorList = [

			ErrorMessages::PRUEBA => 'ERROR'
		];
	}

	public function get($hash){
		return $this->errorList[$hash];
	}

	public function existkey($key){
		if (array_key_exists($key, $this->errorList)) {
			return true;
		}else{
			return false;
		}
	}
}

?>