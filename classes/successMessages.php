<?php

class SuccessMessages{

	const PRUEBA = "eec8c35c345a1cd380c8cb539df27970";

	private $successrList = [];

	public function __construct(){
		$this->successList = [

			SuccessMessages::PRUEBA => 'EXITO'
		];
	}

	public function get($hash){
		return $this->successList[$hash];
	}

	public function existkey($key){
		if (array_key_exists($key, $this->successList)) {
			return true;
		}else{
			return false;
		}
	}
}

?>